import Vue from 'vue'
import Router from 'vue-router'
const _import = require('./_import_' + process.env.NODE_ENV)
// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../view/layout/Layout'

/** note: submenu only apppear when children.length>=1
*   detail see  https://panjiachen.github.io/vue-element-admin-site/#/router-and-nav?id=sidebar
**/

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    roles: ['admin','editor']     will control the page roles (you can set multiple roles)
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
    noCache: true                if true ,the page will no be cached(default is false)
  }
**/
export const constantRouterMap = [
  { path: '/login', component: _import('login/login'), hidden: true },
  { path: '/authredirect', component: _import('login/authredirect'), hidden: true },
  { path: '/404', component: _import('errorPage/404'), hidden: true },
  { path: '/401', component: _import('errorPage/401'), hidden: true },
  
  {
    path: '',
    component: Layout,
    redirect: 'dashboard',
    children: [{
      path: 'dashboard',
      component: _import('dashboard/index'),
      name: 'dashboard',
      meta: { title: '首页', icon: 'dashboard', noCache: true }
    }]
  },
]

export default new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

export const asyncRouterMap = [

  {
    path: '/system',
    component: Layout,
    meta: { perm:'m:sys', title: '系统', icon: 'chart' },
    children: [
      {
        path: 'user_manage',
        name: 'user_manage',
        component: _import('_system/user/index'),
        meta: { perm: 'm:sys:user', title: '用户管理', icon: 'chart', noCache: true }
      },
      {
        path: 'role_manage',
        name: 'role_manage',
        component: _import('_system/role/index'),
        meta: { perm: 'm:sys:role', title: '角色管理', icon: 'chart', noCache: true },
      },
      {
        hidden: true,
        path: 'role_manage/:roleId/assign_perm',
        name: 'role_manage_assign_perm',
        component: _import('_system/role/assign_perm'),
        meta: { hiddenTag: true , title: '角色授权'},
      },
      {
        path: 'perm_manage',
        name: 'perm_manage',
        component: _import('_system/perm/index'),
        meta: { perm: 'm:sys:perm', title: '权限管理', icon: 'chart', noCache: true }

      },
    ]
  }
  ,
  {
    path: '/shop',
    meta: { perm:'m:shop', title: '商户管理', icon: 'icon' },
    component: Layout,
    children: [{
      path: 'index',
      name: 'shop',
      component: _import('shop/index'),
      meta: { perm:'m:shop', title: '商户管理', icon: 'icon' },
    },{
      hidden: true,
      path: 'addShop',
      name: 'addShop',
      component: _import('shop/addShop'),
      meta: { perm: 'm:shop:add', title: '添加商户', icon: 'chart', noCache: true }

    },{
      hidden: true,
      path: 'selectShop',
      name: 'selectShop',
      component: _import('shop/viewShop'),
      meta: { title: '查看商户', icon: 'chart', noCache: true }

    },{
      hidden: true,
      path: 'updateShop',
      name: 'updateShop',
      component: _import('shop/updateShop'),
      meta: { perm: 'm:shop:update', title: '修改商户', icon: 'chart', noCache: true }

    }]
  },
  {
    path: '/indexBanner',
    component: Layout,
    children: [{
      path: 'index',
      name: 'indexBanner',
      component: _import('indexBanner/index'),
      meta: { perm:'m:indexBanner', title: 'Banner管理', icon: 'icon' }
    },]
  },

  {
    path: '/indexessay',
    component: Layout,
    children: [{
      path: 'index',
      name: 'indexessay',
      component: _import('indexessay/index'),
      meta: { perm:'m:indexessay', title: '首页文章管理', icon: 'tab' }
    }]
  },

  {
    path: '/userActivity',
    component: Layout,
    children: [{
      path: 'index',
      name: 'userActivity',
      component: _import('userActivity/index'),
      meta: { perm:'m:userActivity', title: '视频动态管理', icon: 'clipboard' }
    }]
  },

  {
    path: '/pushEssay',
    component: Layout,
    children: [{
      path: 'index',
      name: 'pushEssay',
      component: _import('pushEssay/index'),
      meta: { perm:'m:pushEssay', title: '文章推送', icon: 'bug' }
    }]
  },

  {
    path: '/cityTable',
    component: Layout,
    children: [{
      path: 'index',
      name: 'cityTable',
      component: _import('cityTable/index'),
      meta: { perm:'m:cityTable', title: '城市列表管理', icon: 'form' }
    }]
  },

  {
    path: '/travelGuide',
    meta: { perm:'m:travelGuide', title: '攻略管理', icon: 'theme' },
    component: Layout,
    children: [{
      path: 'index',
      name: 'travelGuide',
      component: _import('travelGuide/index'),
      meta: { perm:'m:travelGuide', title: '攻略管理', icon: 'international' },
    },{
      hidden: true,
      path: 'addTravelGuide',
      name: 'addTravelGuide',
      component: _import('travelGuide/add'),
      meta: { perm: 'm:travelGuide:add', title: '添加攻略', icon: 'chart', noCache: true }

    },{
      hidden: true,
      path: 'selectTravelGuide',
      name: 'selectTravelGuide',
      component: _import('travelGuide/view'),
      meta: { title: '查看攻略', icon: 'chart', noCache: true }

    },{
      hidden: true,
      path: 'updateTravelGuide',
      name: 'updateTravelGuide',
      component: _import('travelGuide/update'),
      meta: { perm: 'm:travelGuide:update', title: '修改攻略', icon: 'chart', noCache: true }

    }]
  },


  {
    path: '/activity',
    meta: { perm:'m:activity', title: '活动管理', icon: 'theme' },
    component: Layout,
    children: [{
      path: 'index',
      name: 'activity',
      component: _import('activity/index'),
      meta: { perm:'m:activity', title: '活动管理', icon: 'peoples' },
    },{
      hidden: true,
      path: 'addActivity',
      name: 'addActivity',
      component: _import('activity/add'),
      meta: { perm: 'm:activity:add', title: '添加活动', icon: 'chart', noCache: true }

    },{
      hidden: true,
      path: 'selectActivity',
      name: 'selectActivity',
      component: _import('activity/view'),
      meta: { title: '查看活动', icon: 'chart', noCache: true }

    },{
      hidden: true,
      path: 'updateActivity',
      name: 'updateActivity',
      component: _import('activity/update'),
      meta: { perm: 'm:activity:update', title: '修改活动', icon: 'chart', noCache: true }

    }]
  },

  {
    path: '/userApply',
    component: Layout,
    children: [{
      path: 'index',
      name: 'userApply',
      component: _import('userApply/index'),
      meta: { perm:'m:userApply', title: '活动报名管理', icon: 'money' }
    }]
  },

  {
    path: '/orderInfo',
    meta: {  perm: 'm:orderInfo',title: '会员管理', icon: 'peoples' },
    component: Layout,
    children: [{
      path: 'index',
      name: 'orderInfo',
      component: _import('orderInfo/index'),
      meta: { perm:'m:orderInfo', title: 'VIP人数统计', icon: 'chart' , noCache: true }
    },{
      path: 'queryOrderInfo',
      name: 'queryOrderInfo',
      component: _import('orderInfo/query'),
      meta: { perm:'m:orderInfo',title: '会员订单查询', icon: 'money', noCache: true },
    },{
      path: 'batchQuery',
      name: 'batchQuery',
      component: _import('orderInfo/batchQuery'),
      meta: { perm:'m:orderInfo',title: '会员订单批量查询', icon: 'money', noCache: true },
    }]
  },

  {
    path: '/statistical',
    meta: {  perm: 'm:statistical',title: '统计报表', icon: 'chart' },
    component: Layout,
    children: [{
      path: 'avtivitySumPeople',
      name: 'avtivitySumPeople',
      component: _import('statistics/avtivitySumPeople'),
      meta: {title: '活动场次及人数统计', icon: 'chart' , noCache: true }
    },{
      path: 'activitySFRatio',
      name: 'activitySFRatio',
      component: _import('statistics/activitySFRatio'),
      meta: {title: '活动成功失败比例', icon: 'chart' , noCache: true }
    },{
      path: 'activityCategory',
      name: 'activityCategory',
      component: _import('statistics/activityCategory'),
      meta: {title: '活动类别参与统计', icon: 'chart' , noCache: true }
    },{
      path: 'maleFemaleRatio',
      name: 'maleFemaleRatio',
      component: _import('statistics/maleFemaleRatio'),
      meta: {title: '用户男女比例', icon: 'chart' , noCache: true }
    },{
      path: 'shopDistribution',
      name: 'shopDistribution',
      component: _import('statistics/shopDistribution'),
      meta: {title: '商户地区分布', icon: 'chart' , noCache: true }
    },{
      path: 'newUser',
      name: 'newUser',
      component: _import('statistics/newUser'),
      meta: {title: '新用户统计', icon: 'chart' , noCache: true }
    }]
  },

  { path: '*', redirect: '/404', hidden: true }
]
