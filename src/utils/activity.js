/**
 * Created by lc on 19/09/09.
 *
 */

export const activityStatus =[{
    value: '0',
    lable: '活动结束'
}, {
    value: '1',
    lable: '活动中'
}, {
    value: '2',
    lable: '活动未开始报名已结束'
}, {
    value: '3',
    lable: '活动报名中'
}, {
    value: '4',
    lable: '活动报名未开始'
}, {
    value: '-1',
    lable: '活动失败'
}];