/**
 * “VIP人数统计”相关接口
 */
import request from '@/utils/request'

export default{


  /**
   * VIP人数统计
   * @param pageParam
   */
  queryUserApply(queryParam) {
    return request({
      url: '/vip/query',
      method: 'post',
      data: {
        ...queryParam
      }
    })
  },
/**
   * 查询订单
   * @param queryParam
   * @param pageParam
   */
  queryOrderInfo(queryParam,pageParam) {
    return request({
      url: '/vip/queryOrderInfo',
      method: 'post',
      data: {
        ...queryParam,
        current: pageParam.current,
        size: pageParam.size
      }
    })
  },
   /**
   * 用户男女比例
   * @param pageParam
   */
  maleFemaleRatio() {
    return request({
      url: '/vip/maleFemaleRatio',
      method: 'post',
    })
  },
   /**
   * 新用户统计
   * @param pageParam
   */
  newUser(queryParam) {
    return request({
      url: '/vip/newUser',
      method: 'post',
      data: {
        ...queryParam
      }
    })
  },
  /**
   * 根据ID查询订单
   * @param queryParam
   */
  queryOrderInfoByID(queryParam) {
    return request({
      url: '/vip/queryOrderInfoByID',
      method: 'post',
      data: {
        ...queryParam
      }
    })
  },
   /**
   * 根据日期查询
   * @param pageParam
   */
  queryOrderInfoByTime(queryParam) {
    return request({
      url: '/vip/queryOrderInfoByTime',
      method: 'post',
      data: {
        ...queryParam
      }
    })
  },
}