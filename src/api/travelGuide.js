/**
 * “攻略管理”相关接口
 */
import request from '@/utils/request'

export default{

  /**
   * 删除攻略
   * @param data
   */
  deleteTravelGuide(data) {
    return request({
      url: '/travelGuide/delete',
      method: 'post',
      data
    })
  },

  /**
   * 查询攻略
   * @param pageParam
   */
  queryTravelGuide(queryParam,pageParam) {
    return request({
      url: '/travelGuide/query',
      method: 'post',
      data: {
        ...queryParam,
        current: pageParam.current,
        size: pageParam.size
      }
    })
  },
  /**
   * 查找攻略
   * @param data
   */
  selectTravelGuide(id) {
    return request({
      url: '/travelGuide/selectTravelGuide?id='+id,
      method: 'get'
    })
  },

}