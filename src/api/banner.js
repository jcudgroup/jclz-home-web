/**
 * “banner管理”相关接口
 */
import request from '@/utils/request'

export default{

  /**
   * 修改banner
   * @param id
   * @param indexBanner
   * @param extraUrl
   * @param bannerText
   * @param typeId
   */
  updateBanner(data) {
    return request({
      url: '/banner/updateBanner',
      headers: {'content-type': 'multipart/form-data'},
      method: 'post',
      data
    })
  },
  selectBanner(){
   return request({
     url:'/banner/selectBanner',
     method:'post'
   }) 
  },
  selectBannerByRow(row){
    return request({
      url:'/banner/selectBannerByRow',
      method:'post'
    })
  },
 /**
   * 删除Banner
   * @param data
   */
  deleteBanner(data) {
    return request({
      url: '/banner/delete',
      method: 'post',
      data
    })
  },


}