/**
 * “商户管理”相关接口
 */
import request from '@/utils/request'

export default{
 /**
   * 添加商户
   * @param data
   */
  addShop(data) {
    return request({
      url: '/shop/addShop',
      headers: {'content-type': 'multipart/form-data'},
      method: 'post',
      data
    })
  },

  /**
   * 删除商户
   * @param data
   */
  deleteShop(data) {
    return request({
      url: '/shop/deleteShop',
      method: 'post',
      data
    })
  },

  /**
   * 查询商户
   * @param queryParam
   * @param pageParam
   */
  queryShop(queryParam,pageParam) {
    return request({
      url: '/shop/query',
      method: 'post',
      data: {
        ...queryParam,
        current: pageParam.current,
        size: pageParam.size
      }
    })
  },
  /**
   * 查找商户
   * @param id
   */
  selectShop(id) {
    return request({
      url: '/shop/selectShop?id='+id,
      method: 'get'
    })
  },
  /**
   * 下架或上架商户
   * @param data
   */
  lowerShelfShop(data) {
    return request({
      url: '/shop/lowerShelfShop',
      method: 'post',
      data
    })
  },

   /**
   * 商户分布统计
   * @param pageParam
   */
  shopDistribution() {
    return request({
      url: '/shop/shopDistribution',
      method: 'post',
    })
  },
}