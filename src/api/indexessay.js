/**
 * “首页文章管理”相关接口
 */
import request from '@/utils/request'

export default{

  /**
   * 删除首页文章
   * @param data
   */
  deleteIndexEssay(data) {
    return request({
      url: '/indexessay/delete',
      method: 'post',
      data
    })
  },

  /**
   * 查询首页文章
   * @param queryParam
   * @param pageParam
   */
  queryIndexEssay(queryParam,pageParam) {
    return request({
      url: '/indexessay/query',
      method: 'post',
      data: {
        ...queryParam,
        current: pageParam.current,
        size: pageParam.size
      }
    })
  },
  /**
   * 查找首页文章
   * @param id
   */
  selectIndexEssay(id) {
    return request({
      url: '/indexessay/select?id='+id,
      method: 'get'
    })
  },

}