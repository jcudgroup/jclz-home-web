/**
 * “视频动态管理”相关接口
 */
import request from '@/utils/request'

export default{

  /**
   * 删除视频动态
   * @param data
   */
  deleteUserActivity(data) {
    return request({
      url: '/userActivity/delete',
      method: 'post',
      data
    })
  },

  /**
   * 查询视频动态
   * @param pageParam
   */
  queryUserActivity(pageParam) {
    return request({
      url: '/userActivity/query',
      method: 'post',
      data: {
        current: pageParam.current,
        size: pageParam.size
      }
    })
  },
/**
   * 发布动态的用户id
   * @param data
   */
  selectUserID() {
    return request({
      url: '/userActivity/selectUserID',
      method: 'post'
    })
  },
   /**
   * 修改用户动态是否精选
   * @param data
   */
  updateUserActityHot(data) {
    return request({
      url: '/userActivity/updateUserActityHot',
      method: 'post',
      data
    })
  },

   /**
   * 审核用户动态
   * @param data
   */
  updateUserActityCheck(data) {
    return request({
      url: '/userActivity/updateUserActityCheck',
      method: 'post',
      data
    })
  },
}