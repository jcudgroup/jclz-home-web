/**
 * “活动管理”相关接口
 */
import request from '@/utils/request'

export default{

  /**
   * 删除活动
   * @param data
   */
  deleteActivity(data) {
    return request({
      url: '/activity/delete',
      method: 'post',
      data
    })
  },

  /**
   * 查询活动
   * @param pageParam
   */
  queryActivity(queryParam,pageParam) {
    return request({
      url: '/activity/query',
      method: 'post',
      data: {
        ...queryParam,
        current: pageParam.current,
        size: pageParam.size
      }
    })
  },
  /**
   * 查找活动
   * @param data
   */
  selectActivity(id) {
    return request({
      url: '/activity/selectActivity?id='+id,
      method: 'get'
    })
  },

   /**
   * 活动场次及人数统计
   * @param pageParam
   */
  queryStatements(queryParam) {
    return request({
      url: '/activity/queryStatements',
      method: 'post',
      data: {
        ...queryParam
      }
    })
  },
   /**
   * 活动成功失败比例
   * @param pageParam
   */
  activitySFRatio(queryParam) {
    return request({
      url: '/activity/activitySFRatio',
      method: 'post',
      data: {
        ...queryParam
      }
    })
  },
   /**
   * 活动类别统计
   * @param pageParam
   */
  activityCategory(queryParam) {
    return request({
      url: '/activity/activityCategory',
      method: 'post',
      data: {
        ...queryParam
      }
    })
  },
}