/**
 * “文章推送”相关接口
 */
import request from '@/utils/request'

export default{
 /**
   * 文章推送
   * @param data
   */
  pushEssay(data) {
    return request({
      url: '/push/essay',
      method: 'post',
      data
    })
  },

}