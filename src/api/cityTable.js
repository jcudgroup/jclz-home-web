/**
 * “首页文章管理”相关接口
 */
import request from '@/utils/request'

export default{

  /**
   * 查询城市列表
   * @param queryParam
   * @param pageParam
   */
  queryCityTable(queryParam,pageParam) {
    return request({
      url: '/cityTable/query',
      method: 'post',
      data: {
        ...queryParam,
        current: pageParam.current,
        size: pageParam.size
      }
    })
  },
  /**
   * 查询父级城市
   *
   */
  selectFCity() {
    return request({
      url: '/cityTable/selectFCity',
      method: 'post'
    })
  },
/**
   * 查询子级城市
   *
   */
  selectSunCity() {
    return request({
      url: '/cityTable/selectSunCity',
      method: 'post'
    })
  },
  /**
   * 修改城市开放状态
   * @param data
   */
  updateCityStatus(data) {
    return request({
      url: '/cityTable/updateCityStatus',
      method: 'post',
      data
    })
  },
}