/**
 * “活动报名管理”相关接口
 */
import request from '@/utils/request'

export default{


  /**
   * 查询活动报名
   * @param pageParam
   */
  queryUserApply(queryParam,pageParam) {
    return request({
      url: '/userApply/query',
      method: 'post',
      data: {
        ...queryParam,
        current: pageParam.current,
        size: pageParam.size
      }
    })
  },
  /**
   * 是否允许退款
   * @param data
   */
  updateIsRefund(data) {
    return request({
      url: '/userApply/updateIsRefund',
      method: 'post',
      data
    })
  },

}